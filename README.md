# Infection: A Heuristic for Spreading Software Updates

Welcome to `infection`! This is a small project demonstrating a set of
heuristics for passing on software updates to connected members of a
software community/site.

For now, this is strictly the core algorithmic components. A full
implementation would feature connectors to external storage and data
models.

This project is implemented in Haskell, so building these sources and
running examples requires a recent Haskell compiler.

## Install GHC/Haskell

1. Install [stack](http://docs.haskellstack.org/en/stable/README.html#how-to-install), the Haskell Tool Stack
2. Change directory into this project
3. Run `stack setup`
4. You now have a compatible GHC/Haskell compiler installed

## Testing

### Doctests

Doctests are included primarily to aid users in understanding how to
interact with the included modules. To execute the doctests, run:

```
$ stack test infection:doctest
```

If you'd like to reduce build output, (for example, editor integration
purposes), run as:

```
$ stack --verbosity 0 test infection:doctest
```

### Property Tests

Property tests leverage the excellent
[QuickCheck](https://hackage.haskell.org/package/QuickCheck) library
in order to determine that certain properties hold for given
implementations.

In order to run them:

```
$ stack test infection:property
```

### (micro) Benchmarks

Benchmarks utilize the
[criterion](http://www.serpentine.com/criterion/) library, which
reduces the impact of noise (garbage collection, other system
processes, etc.) on run outcomes using statistical techniques.

To run them:

```
$ stack bench
```

## Documentation

To build the documentation:

```
$ stack haddock
```

Once the (long) process completes, it should output a link to where
the HTML was (locally) written to.
