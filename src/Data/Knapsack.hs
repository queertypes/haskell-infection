-- |
-- Module: Data.Knapsack
-- Description: Algorithms for dealing with knapsack (subset sum) problems
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
module Data.Knapsack (
  greedy,
  Limit(..)
) where

import Data.Foldable (foldl')
import Data.List (sortOn)
import Data.Word (Word64)

newtype Limit = Limit Word64 deriving Show

-- |
-- A greedy algorithm for solving the subset sum problem within OPT(n/2)
--
-- >>> greedy [("dog1",50),("dog2",50)] (Limit 100)
-- (100,["dog1","dog2"])
-- >>> greedy [("cat",51),("dog1",50),("dog2",50)] (Limit 100)
-- (51,["cat"])
-- >>> greedy [] (Limit 100)
-- (0,[])
greedy :: Ord a => [(a,Word64)] -> Limit -> (Word64,[a])
greedy xs (Limit limit) =
  (\(w,_,xs) -> (w,xs)) $ foldl' step (0,limit,[]) (reverse $ sortOn snd xs)
  where step (accW, lim, acc) (x,w)
          | (accW + w) <= lim = (accW+w, lim, x:acc)
          | otherwise = (accW, lim, acc)
