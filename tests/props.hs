-- |
-- Module: Main
-- Description: Property test suite for infection
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
--
-- To run:
--
-- $ stack test infection:property
{-# LANGUAGE DataKinds, FlexibleInstances #-}
module Main where

import Data.Word (Word64)
import Data.Text (Text, pack)
import Data.UUID
import Test.Tasty
import Test.Tasty.QuickCheck
import qualified Data.Set as S

import Data.Simple.Graph
import Data.Infection
import Data.Knapsack
import Data.User

--------------------------------------------------------------------------------
--                       Generating Examples                                  --
--------------------------------------------------------------------------------
instance Arbitrary UUID where
  arbitrary = fromWords <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary Text where
  arbitrary = pack <$> arbitrary

instance Arbitrary Version where
  arbitrary = Version <$> arbitrary

instance Arbitrary User where
  arbitrary = User <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance (Ord v, Arbitrary v) => Arbitrary (Graph d v) where
  arbitrary = fromList <$> arbitrary

instance Arbitrary Color where
  arbitrary = elements [W, G, B]

instance Arbitrary Limit where
  arbitrary = Limit <$> arbitrary

--------------------------------------------------------------------------------
--                              Tests                                         --
--------------------------------------------------------------------------------
reachableUsersInfected :: [User] -> Bool
reachableUsersInfected us =
  let g = userGraph us
      u = head us
      reachableNodes u' g' = filter (\v -> (v `reachableFrom` u') g') (vertices g')
      infectedNodes = totalInfection u us
  in S.fromList (reachableNodes u g) == S.fromList infectedNodes

knapsackGreedyNeverExceedsLimit :: [(Text,Word64)] -> Limit -> Bool
knapsackGreedyNeverExceedsLimit xs limit@(Limit lim) =
  fst (greedy xs limit) <= lim

testsKnapsack :: TestTree
testsKnapsack = testGroup "total"
  [ testProperty "solutions always within LIMIT" knapsackGreedyNeverExceedsLimit
  ]

testsInfectionTotal :: TestTree
testsInfectionTotal = testGroup "total"
  [ testProperty "all reachable users infected" reachableUsersInfected
  ]

main :: IO ()
main = defaultMain $ testGroup "main"
  [
    testGroup "infection" [testsInfectionTotal]
  , testGroup "knapsack" [testsKnapsack]
  ]
