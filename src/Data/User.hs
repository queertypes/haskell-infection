-- |
-- Module: Data.User
-- Description: User data model
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
--
-- The user data model.
--
-- At a minimum, we track the following for users:
-- * user id
-- * name
-- * site version they're running
--
-- This corresponds to SQL akin to:
--
-- @
-- CREATE TABLE IF NOT EXISTS user (
--   id uuid PRIMARY KEY NOT NULL,
--   name text NOT NULL,
--   siteVersion int NOT NULL,
-- );
-- @
--
-- Further, if a user is coaching someone, records can be found
-- linking them in the `coaches` table:
--
-- @
-- CREATE TABLE IF NOT EXISTS coaches (
--   coachId uuid PRIMARY KEY NOT NULL REFERENCES (user.id),
--   coachingId uuid NOT NULL REFERENCES (user.id)
--   CHECK (coachId != coachingId)
-- );
-- @
--
-- Self-loops are prevented at the persistence layer to simplify the
-- application layer checks.
module Data.User where

import Data.Word (Word8)
import Data.Text (Text, unpack)
import Data.UUID (UUID)

newtype Version = Version Word8 deriving Show
type UserId = UUID

data User
  = User { uuid :: UserId
         , name :: Text
         , siteVersion :: Version
         , coaches :: [UserId]
         }

instance Eq User where
  l == r = (uuid l) == (uuid r)

instance Ord User where
  l <= r = (uuid l) <= (uuid r)

instance Show User where
  show (User uid n v cs) =
    "(User id:" ++ show uid
    ++ " name:" ++ unpack n
    ++ " version:" ++ show v
    ++ " coaches:" ++ show cs ++ ")"

-- for doctests
newtype SimpleUser = SimpleUser User

instance Show SimpleUser where
  show (SimpleUser (User _ n _ _)) = "User " ++ unpack n
