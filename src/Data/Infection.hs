{-# LANGUAGE DataKinds #-}
-- |
-- Module: Data.Infection
-- Description: Infection algorithms for user graphs
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
module Data.Infection (
  userGraph,
  totalInfection,
  limitedInfection
) where

import Data.Function (on)
import Data.List (maximumBy)
import Data.Foldable (foldl')
import qualified Data.Map as M
import qualified Data.Set as S

import Data.Simple.Graph (Graph, Directedness(..))
import Data.User
import qualified Data.Simple.Graph as G


newtype UpperLimit = UpperLimit Int

-- |
-- /O(n * log n)/.
--
-- Given a list of users, builds a user graph from them.
userGraph :: [User] -> Graph 'Directed User
userGraph users =
  let userIdMap = foldl' (\acc u -> M.insert (uuid u) u acc) M.empty users
      userAdjList = foldl' (\acc u -> (u,adj u):acc) [] users
      adj u =
        foldl' (\acc uid -> maybe acc (:acc) $ M.lookup uid userIdMap) [] (coaches u)
  in G.fromList userAdjList

-- |
-- /O(n * log n + E * log E)/
--
-- Starting with a user, infect all users connected to this one.
--
-- In this example, there's a user graph that looks like:
--
-- @
--    1      4
--   / \\
--  v   v
--  2   3
-- @
--
-- >>> import Data.UUID (fromWords)
-- >>> let uid1 = fromWords 0 0 0 0
-- >>> let uid2 = fromWords 0 0 0 1
-- >>> let uid3 = fromWords 0 0 0 2
-- >>> let uid4 = fromWords 0 0 0 3
-- >>> :set -XOverloadedStrings
-- >>> let user1 = User uid1 "a" (Version 1) [uid2, uid3]
-- >>> let user2 = User uid2 "b" (Version 1) []
-- >>> let user3 = User uid3 "c" (Version 1) []
-- >>> let user4 = User uid4 "d" (Version 1) []
-- >>> map SimpleUser $ totalInfection user1 [user1, user2, user3, user4]
-- [User a,User b,User c]
-- >>> map SimpleUser $ totalInfection user2 [user1, user2, user3, user4]
-- [User a,User b,User c]
-- >>> map SimpleUser $ totalInfection user4 [user1, user2, user3, user4]
-- [User d]
totalInfection :: User -> [User] -> [User]
totalInfection start users =
  let g = G.undirected (userGraph users)
      bfsTree = G.bfs g start
      willInfect = S.intersection (S.fromList users) (S.fromList $ map fst bfsTree)
  in S.toList willInfect

-- |
-- Starting with a user, infects at most UpperLimit users
--
-- This assumes that all users in the given user list form a connected
-- graph. A means to relax this assumption is to decompose the graph
-- into strongly-connected components and assign a weight to each
-- component. However, due to limited time, this avenue will not be
-- pursued at this time.
--
-- The algorithm is as follows:
--
--   * Construct a BFS tree of the users rooted at the user who
--     coaches the greatest number of other users
--   * Take as many users as needed to reach the UpperLimit
--
-- Some benefits to this approach:
--   * Biased towards getting all students for a given teacher,
--     because of BFS construction
--   * Simple
--   * If there are users to meet the limit, infects them all
--   * Will never exceed the given limit
--
-- Given that this algorithm has a severe weakness (assumes list of
-- users given is strongly connected, here are some problems that will
-- crop up in practice:
--
-- Notable problems:
--
--   * May infect less users than optimal depending on chosen root
--     - a root coaching many may lead to a subgraph that includes few users
--   * Assumes list of all users to consider will fit in memory
--      - Has no incrementalism to it
--
-- TODO: fix all that
--
-- Returns a list of users to infect, guaranteeing (length xs <=
-- UpperLimit)
--
-- Starting with a user, infect all users connected to this one.
--
-- Let's revisit the previous user graph
--
-- @
--    1      4
--   / \\
--  v   v
--  2   3
-- @
--
-- >>> import Data.UUID (fromWords)
-- >>> let uid1 = fromWords 0 0 0 0
-- >>> let uid2 = fromWords 0 0 0 1
-- >>> let uid3 = fromWords 0 0 0 2
-- >>> let uid4 = fromWords 0 0 0 3
-- >>> :set -XOverloadedStrings
-- >>> let user1 = User uid1 "a" (Version 1) [uid2, uid3]
-- >>> let user2 = User uid2 "b" (Version 1) []
-- >>> let user3 = User uid3 "c" (Version 1) []
-- >>> let user4 = User uid4 "d" (Version 1) []
-- >>> limitedInfection [] (UpperLimit 10)
-- []
-- >>> map SimpleUser $ limitedInfection [user1, user2, user3, user4] (UpperLimit 2)
-- [User a,User b]
-- >>> map SimpleUser $ limitedInfection [user1, user2, user3, user4] (UpperLimit 1)
-- [User a]
limitedInfection :: [User] -> UpperLimit -> [User]
limitedInfection [] _ = []
limitedInfection users@(_:_) (UpperLimit limit) =
  let g = G.undirected (userGraph users)
      studentCount = length . coaches
      start = maximumBy (compare `on` studentCount) users
      bfsTree = G.bfs g start
  in take limit (map fst bfsTree)
