-- |
-- Module: Main
-- Description: Microbenchmark suite for infection
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
--
-- To run:
--
-- $ stack bench
module Main where

import Criterion.Main
import Data.Monoid

import Data.Simple.Graph

graph1 :: Graph d Int
graph1 = fromList
  [ (1, [2,3])
  , (2, [])
  , (3, [])
  ]

completeGraph :: Graph d Int
completeGraph = fromList $ zip [1..1000] (repeat [1..1000])

disconnectedGraph :: Graph d Int
disconnectedGraph = fromList
  ((zip [1..20] (repeat [3..6]))
  <> (zip [21..40] (repeat [23..26]))
  <> (zip [41..100] (repeat [57..75])))


graphBench :: [Benchmark]
graphBench =
  [ bgroup "undirected" $
    [ bench "simple" $ nf undirected graph1
    , bench "complete" $ nf undirected completeGraph
    , bench "disconnected" $ nf undirected disconnectedGraph
    ]

  , bgroup "transpose" $
    [ bench "simple" $ nf transpose graph1
    , bench "complete" $ nf transpose completeGraph
    , bench "disconnected" $ nf transpose disconnectedGraph
    ]

  , bgroup "bfs" $
    [ bench "simple" $ nf (bfs graph1) 1
    , bench "complete" $ nf (bfs completeGraph) 1
    , bench "disconnected" $ nf (bfs disconnectedGraph) 1
    ]
  ]

main :: IO ()
main = defaultMain
  [
    bgroup "graphs" graphBench
  ]
