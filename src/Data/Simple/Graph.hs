{-# LANGUAGE ConstraintKinds, KindSignatures, DataKinds, DeriveGeneric, DeriveAnyClass #-}
-- |
-- Module: Data.Simple.Graph
-- Description: Graph data structures and algorithms
-- Copyright: (c) Allele Dev, 2016
-- License: BSD-3
-- Maintainer: allele.dev@gmail.com
-- Stability: experimental
module Data.Simple.Graph (
  Graph,
  RoseTree(..),
  Color(..),
  Directedness(..),

  fromList,
  toList,
  fromEdges,
  undirected,
  transpose,
  vertices,
  edges,
  outdegree,
  reachableFrom,
  valid,
  bfs
) where

import Control.DeepSeq
import Data.Monoid
import Data.List ((\\))
import Data.Foldable (foldl')
import Data.Maybe
import Data.Sequence (viewr, ViewR(..), singleton)
import GHC.Generics (Generic)
import qualified Data.Map.Strict as M
import qualified Data.Sequence as S
import qualified Data.Set as Set

--------------------------------------------------------------------------------
--                                Types                                       --
--------------------------------------------------------------------------------
type Mapping v = M.Map v [v]

data Directedness = Directed | Undirected

data Graph (d :: Directedness) v
  = Graph (Mapping v)
  deriving (Show, Eq, Generic, NFData)

instance (Ord v) => Monoid (Graph d v) where
  mempty = Graph mempty
  mappend (Graph l) (Graph r) = Graph (M.unionWith (<>) l r)


data RoseTree v
  = RoseTree !v [RoseTree v]
    deriving Show

data Color
  = W
  | G
  | B
    deriving (Show, Eq, Ord)

data Colored v =
  Colored !v !Color
  deriving (Show, Eq)

type Cap v = (Ord v)
type Edge v = (v,v)

--------------------------------------------------------------------------------
--                             Helpers                                        --
--------------------------------------------------------------------------------
fromEdges' :: Cap v => [(v,v)] -> Mapping v
fromEdges' vs = foldl' step initMap vs
  where initMap =
          let allNodes =
                foldl' (\acc (x,y) -> x : y : acc) [] vs
              uniq = (Set.toList . Set.fromList) allNodes
          in M.fromList (zip uniq (repeat []))
        step acc (k,v) = M.adjust (v:) k acc

isWhite :: Cap v => v -> M.Map v Color -> Bool
isWhite v cs = maybe False (== W) (M.lookup v cs)

--------------------------------------------------------------------------------
--                            Interface                                       --
--------------------------------------------------------------------------------

-- |
-- Generates a directed graph from an adjacency list
fromList :: Cap v => [(v, [v])] -> Graph d v
fromList = Graph . foldl' (\m (k,v) -> M.insert k v m) M.empty

-- |
-- Converts a graph to an adjacency list
toList :: Graph d v -> [(v, [v])]
toList (Graph m) = M.toList m

-- |
-- /O(n * log n)/. Constructs a graph from a list of edges.
--
-- >>> fromEdges [(1,2),(2,3),(3,1),(1,4),(2,4),(3,4)]
-- Graph (fromList [(1,[4,2]),(2,[4,3]),(3,[4,1]),(4,[])])
fromEdges :: Cap v => [(v,v)] -> Graph d v
fromEdges = Graph . fromEdges'

-- |
-- >>> vertices (fromList [(1,[2,3]), (2,[]), (3,[])])
-- [1,2,3]
vertices :: Graph d v -> [v]
vertices (Graph m) = M.keys m

-- |
-- Generate a list of edges for a graph
--
-- >>> edges (fromList [(1,[2,3]), (2,[3]), (3,[1])])
-- [(3,1),(2,3),(1,3),(1,2)]
-- >>> edges (fromList [(1,[2,4]), (2,[3]), (3,[1]), (4,[])])
-- [(3,1),(2,3),(1,4),(1,2)]
edges :: Graph d v -> [Edge v]
edges (Graph m) = go (M.toList m) []
  where go ((x,xs):xss) acc = go xss (foldl' (\a n -> (x,n) : a) acc xs)
        go [] acc = acc

-- |
-- Returns all nodes adjacent to a node v
--
-- >>> let g = fromList [(1,[1,2,3]), (2,[]), (3,[])]
-- >>> adjacent g 1
-- Just [1,2,3]
-- >>> adjacent g 2
-- Just []
-- >>> adjacent g 4
-- Nothing
adjacent :: Cap v => Graph d v -> v -> Maybe [v]
adjacent (Graph m) v = M.lookup v m

-- |
-- Converts any graph into an undirected graph
--
-- >>> undirected (fromList [(1,[2]), (2,[]), (3,[2,1])])
-- Graph (fromList [(1,[2,3]),(2,[1,3]),(3,[2,1])])
-- >>> undirected (fromList [(1,[2,3]),(2,[]),(3,[]),(4,[])])
-- Graph (fromList [(1,[2,3]),(2,[1]),(3,[1]),(4,[])])
undirected :: Cap v => Graph 'Directed v -> Graph 'Undirected v
undirected g = mkUndirected (g <> transpose g)
  where mkUndirected (Graph m) = Graph m

-- |
--
-- 1. Get all edges from the graph
-- 2. Flip the arrows, e.g., (u,v) -> (v,u)
-- 3. For each vertex v in the edge list:
--   * Build an empty adjacency list
--   * Populate the adjacency lists with all u such that (v,u) exists
-- 4. Find all vertices not covered by the edge list
--   (all vertices without edges to them in the original graph)
-- 5. Build empty adjacency lists for these and add them to the graph
-- 6. Return the transposed graph
--
-- >>> let g = fromList [(1,[2,3]), (2,[]), (3,[2,1])]
-- >>> transpose g
-- Graph (fromList [(1,[3]),(2,[1,3]),(3,[1])])
-- >>> let g = fromList [(1,[2,3]), (2,[4]), (3,[5]), (4,[]), (5,[])]
-- >>> transpose g
-- Graph (fromList [(1,[]),(2,[1]),(3,[1]),(4,[2]),(5,[3])])
transpose :: Cap v => Graph 'Directed v -> Graph 'Directed v
transpose g =
  let edgeMap = (fromEdges' . map (\(k,v) -> (v,k)) . edges) g
      missingVerts = (vertices g \\ M.keys edgeMap)
      adjMap = M.fromList (zip missingVerts (repeat []))
  in Graph (edgeMap <> adjMap)

-- |
-- Determines whether a path exists from u to v in (g :
-- Graph). Performs a bfs rooted at u to determine whether v is
-- reachable.
--
-- Below is an example consisting of a graph made of disconnected sets:
--   [1 -> 2 -> 3]
--   [4 -> 5 -> 6]

-- >>> let g = fromList [(1,[2]), (2,[3]), (3,[]), (4,[5]), (5,[6]), (6,[])]
-- >>> (1 `reachableFrom` 3) g
-- False
-- >>> (3 `reachableFrom` 1) g
-- True
-- >>> (4 `reachableFrom` 1) g
-- False
-- >>> (6 `reachableFrom` 4) g
-- True
reachableFrom :: Cap v => v -> v -> Graph d v -> Bool
reachableFrom u v g = v `elem` map fst (bfs g u)

-- |
-- Returns the number of edges going out from v in g, if v is in g
--
-- >>> let g = fromList [(1,[2,3]),(2,[]),(3,[])]
-- >>> outdegree 1 g
-- Just 2
-- >>> outdegree 2 g
-- Just 0
-- >>> outdegree 4 g
-- Nothing
outdegree :: Ord v => v -> Graph d v -> Maybe Int
outdegree v g = fmap length (adjacent g v)

-- |
-- /O(n^2)/. Verifies that a graph is valid. This includes:
-- * All nodes mentioned in adjacency lists are members of the graph
--
-- >>> valid (fromList [(1,[2])])
-- False
-- >>> valid (fromList [(1,[2]),(2,[])])
-- True
valid :: Ord v => Graph d v -> Bool
valid g =
  let adjacencies = concatMap (fromJust . adjacent g) (vertices g)
      outNodes = foldl' (flip (:)) [] adjacencies
      inNodes = vertices g
  in all (`elem` inNodes) outNodes

-- |
--
-- Performs a breadth first search in a given graph rooted at a given node.
--
-- Here's a rather connected graph:
--
-- @
-- 1 -> [2,3]
-- 2 -> [3]
-- 3 -> [1,4,5]
-- 4 -> [2,3]
-- 5 -> [1,2]
-- @
--
-- >>> let g = fromList [(1, [2,3]), (2, [3]), (3, [1,4,5]), (4, [2,3]), (5, [1,2])]
-- >>> bfs g 1
-- [(1,0),(3,1),(2,1),(5,2),(4,2)]
-- >>> bfs g 2
-- [(2,0),(3,1),(5,2),(4,2),(1,2)]
--
-- The following graph is a diamond structure. It verifies that vertex
-- 4 is added only once, even though 2 and 3 are discovered at the
-- same step:
--
-- @
--    1
--   / \\
--  v   v
--  2   3
--   \\ /
--    v
--    4
-- @
--
-- >>> bfs (fromList [(1,[2,3]), (2,[4]), (3,[4]), (4,[])]) 1
-- [(1,0),(3,1),(2,1),(4,2)]
--
-- Here's a BFS over a graph with an isolated node:
--
-- @
--     1      4
--    / \\
--   v   v
--   2   3
-- @
--
-- >>> bfs (fromList [(1,[2,3]),(2,[]),(3,[]),(4,[])]) 1
-- [(1,0),(3,1),(2,1)]
-- >>> bfs (fromList [(1,[2,3]),(2,[]),(3,[]),(4,[])]) 4
-- [(4,0)]
bfs :: Cap v => Graph d v -> v -> [(v,Int)]
bfs graph root =
  let initColoring = (M.fromList $ zip (vertices graph) (repeat W))
  in go graph (singleton (root,0)) initColoring
  where go g queue coloring =
          case viewr queue of
            EmptyR -> []
            (queue' :> (v,depth)) ->
              let next =
                    (`zip` repeat (depth+1)) (
                      filter (`isWhite` coloring) (fromJust $ adjacent g v))
                  coloring' =
                    M.adjust (const B) v (  -- mark v black:visited
                      foldr (M.adjust (const G) . fst) coloring next)  -- mark u gray:discovered
              in (v,depth) : go g (S.fromList next <> queue') coloring'
